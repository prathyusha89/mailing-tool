import { Injectable, OnDestroy } from "@angular/core";
import { Observable, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { NotificationInfo } from "../models/notification-info.model";
import { RestcallsService } from "../services/restcalls.service";

@Injectable({
  providedIn: "root",
})
export class APIConnector implements OnDestroy {
  private destroy$: Subject<any> = new Subject();

  constructor(private _restCallService: RestcallsService) {}

  public fetchInProgressActivities(
    startDate: string,
    endDate: string
  ): Observable<NotificationInfo[]> {
    return this._restCallService.searchActivities(startDate, endDate).pipe(takeUntil(this.destroy$));;
  }

  public fetchIndashboardActivities(): Observable<NotificationInfo[]> {
    return this._restCallService.dashboardActivities().pipe(takeUntil(this.destroy$));;
  }

  public sendUpdates(request: NotificationInfo): void {
    this._restCallService.sendExtension(request);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
