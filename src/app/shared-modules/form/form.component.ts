import { Component, Input, Output, EventEmitter } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { CONSTANTS } from "../constants";

@Component({
  selector: "app-form",
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.scss"],
})
export class FormComponent {
  @Input() mainForm: FormGroup;
  @Input() isUpdate: boolean;
  @Output() viewUpdates: EventEmitter<boolean> = new EventEmitter();
  private ddlApp = CONSTANTS.dropDownValues.application;
  private ddlEnvironment = CONSTANTS.dropDownValues.environment;
  private ddlActivity = CONSTANTS.dropDownValues.activities;
  private headings = CONSTANTS.activityHeadings;
  constructor() {}

  public onClick() {
    this.viewUpdates.emit(true);
  }
}
