export const CONSTANTS = {
  apiGateway: {
    postFormUrl: "http://localhost:9097/sendmail/create",
    getAllCupJobs: "http://localhost:8080/getallcupjobs",
    getAllActivity: "http://localhost:9097/listAllData",
    dashboardgetAllActivity: "http://localhost:9097/listActivities",
    postReminderUrl: "http://localhost:9097/reminder",
    postExtension: "http://localhost:9097/update/update",
    postEnd: "http://localhost:9097/end/end"
  },
  homeComponentHeaders: {
    create: "CREATE",
    reminder: "REMINDER",
    extensions: "EXTENSIONS",
    end: "ENDED",
  },
  dropDownValues: {
    application: ["CUP", "UDH"],
    environment: ["TEST-A", "TEST-B", "TEST-C", "INT", "PROD"],
    activities: [
      "Planned Downtime",
      "Unplanned Downtime",
      "Impairments",
      "Maintenance",
    ],
  },
  searchActivityHeadings: {
    searchTitle: 'Please select the Date Range to search the activities in Progress:',
    startDate: 'From Date:',
    endDate: 'To Date:',
    search: 'SEARCH',
    reset: 'RESET'
  },
  activityHeadings: {
    app: 'Application:',
    env: 'Environment:',
    desc: 'Description:',
    details: 'Details:',
    activity: 'Activity:',
    updates: 'Updates:',
    reminder: 'Reminder',
    btnUpdate: 'Update',
    btnPreview: 'View Updates',
    end: 'End',
    nextInfo: 'Next Info',
    ticketSystem: 'Ticket System'
  },
  formHeadings: {
    formTitle: 'Lufthansa CUP & UDH Operations',
    information: 'Dear Ladies and Gentlemen,',
    reminder: 'Send Reminder',
    create: 'Send',
    update: 'Send Update',
    footerTemplate: `<p> Apologies for any inconvenience. For any issues / queries regarding the activity, kindly get in touch
    with the CUP/UDH Operations team.<br />
    <br/>OP’s Team 1: +91 88862 83377<br />
    OP’s Team 2: +91 88862 83366<br />
    <br/>Kind regards,<br />
    CUP/UDH Operations Team.
    </p>`,
    footer: `ALL RIGHTS RESERVED BY CGI@2020`,
    end: 'End'
  },
  serverErrorMessage: "There is some problem with the service,Please try again in sometime",
};
