import {
  Component,
  OnInit,
  Output,
  EventEmitter,
} from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { NotificationInfo } from "../../models/notification-info.model";
import { Observable, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { APIConnector } from "../api-connector";

@Component({
  selector: "app-search-activity-container",
  templateUrl: "./search-activity-container.component.html",
})
export class SearchActivityContainerComponent implements OnInit {
  @Output() _notificationInfoObject: EventEmitter<any> = new EventEmitter<any>();
  public searchForm: FormGroup;
  public notificationInfo$: Observable<NotificationInfo[]>;
  
  constructor( private connector: APIConnector) {}

  ngOnInit(): void {
    this.initSearchForm();
  }

  public initSearchForm(): void {
    this.searchForm = new FormGroup({
      startDate: new FormControl(null, [Validators.required]),
      endDate: new FormControl(null, [Validators.required]),
    });
  }

  public onSearchActivity(_search: boolean): void {
    if (_search) {
      this.notificationInfo$ = this.connector.fetchInProgressActivities(this.searchForm.controls["startDate"].value, this.searchForm.controls["endDate"].value);
      this._notificationInfoObject.emit(this.notificationInfo$);
    }
  }
}
