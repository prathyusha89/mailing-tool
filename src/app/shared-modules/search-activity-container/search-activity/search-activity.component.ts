import { Component, OnDestroy, OnInit, Output, EventEmitter, Input } from "@angular/core";
import { CONSTANTS } from "../../constants";
import { FormGroup } from "@angular/forms";


@Component({
  selector: "app-search-activity",
  templateUrl: "./search-activity.component.html",
  styleUrls: ["./search-activity.component.scss"],
})

export class SearchActivityComponent implements OnInit {
  @Input() searchForm: FormGroup;
  @Output() _btnClick: EventEmitter<boolean> = new EventEmitter<boolean>();
  private currentDate: string;
  private headings = CONSTANTS.searchActivityHeadings;

  ngOnInit(): void {
      this.initMinDate();
  }
  
  public initMinDate(): void {
    this.currentDate = new Date().toISOString().split("T")[0];
  }

  public onReset(): void {
    this.searchForm.reset();
    this._btnClick.emit(false);
  }

  public onSearchActivity(): void {
    this._btnClick.emit(true);
  }

}
