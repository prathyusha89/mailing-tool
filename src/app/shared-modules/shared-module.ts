import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { FormComponent } from "./form/form.component";
import { SearchActivityContainerComponent } from "./search-activity-container/search-activity-container.component";
import { SearchActivityComponent } from "./search-activity-container/search-activity/search-activity.component";
import { ModalComponent } from "./modal-popup/modal.component";
import { MaterialModule } from "./material.module";

@NgModule({
    declarations:[
        FormComponent,
        SearchActivityContainerComponent,
        SearchActivityComponent,
        ModalComponent
    ],
    exports:[
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        FormComponent,
        SearchActivityContainerComponent,
        MaterialModule
    ],
    imports:[
        CommonModule,
        FormsModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: "never" })
    ]
})
export class SharedModule {}
