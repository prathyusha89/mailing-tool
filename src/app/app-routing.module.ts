import { NgModule } from "@angular/core";
import { Routes, RouterModule, PreloadAllModules } from "@angular/router";
import { HomeComponent } from "./main/components/home/home.component";
import { ErrorPageComponent } from "./error-page/error-page.component";
import { ReminderContainerComponent } from "./reminder-container/reminder-container.component";
import { CreateComponent } from "./create/create.component";
import { ActivityExtensionsContainerComponent } from "./activity-extensions/activity-extensions-container/activity-extensions-container.component";
import { EndContainerComponent } from "./end-container/end-container.component";

const routes: Routes = [
  {
    path: "",
    redirectTo: '/home',
    pathMatch: "full"
  },
  {
    path: "home",
    component: HomeComponent
  },
  {
    path: "endform",
    component: EndContainerComponent
  },
  {
    path: "errorpage",
    component: ErrorPageComponent
  },
  {
    path: "reminder",
    component: ReminderContainerComponent
  },
  {
    path: "create",
    component: CreateComponent
  },
  {
    path: "extension",
    component: ActivityExtensionsContainerComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
})
export class AppRoutingModule { }


