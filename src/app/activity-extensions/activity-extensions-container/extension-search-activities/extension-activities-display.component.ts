import { Component, Input, Output, EventEmitter } from "@angular/core";
import { NotificationInfo } from "src/app/models/notification-info.model";
import {CONSTANTS} from "../../../shared-modules/constants";
import { Router } from "@angular/router";

@Component({
  selector: "app-extension-activities-display",
  templateUrl: "./extension-activities-display.component.html",
  styleUrls: ["./extension-activities-display.component.scss"],
})

export class ExtensionActivitiesDisplayComponent {
  @Input() _notificationInfo : NotificationInfo[];
  @Output() _activityInfo: EventEmitter<any> =  new EventEmitter<any>();

  private headings = CONSTANTS.activityHeadings;

  constructor(private router: Router) {}

  public selectActivity(obj: NotificationInfo): void {
    this._activityInfo.emit(obj);
  }

}
