import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Observable } from "rxjs";
import { NotificationInfo } from "../../models/notification-info.model";

@Component({
  selector: "app-activity-extensions-container",
  templateUrl: "./activity-extensions-container.component.html",
})
export class ActivityExtensionsContainerComponent implements OnInit {
  activityForm: FormGroup;
  public _notificationInfo$: Observable<NotificationInfo[]>;
  public isSearchPanelVisible = true;
  public updateFormFieldValues: string[];
  public _htmlView: string;

  ngOnInit(): void {
    this.initForm();
  }

  public initForm(): void {
    this.activityForm = new FormGroup({
      application: new FormControl(null, [Validators.required]),
      environment: new FormControl(null, [Validators.required]),
      activity: new FormControl(null, [Validators.required]),
      start: new FormControl(null, [Validators.required]),
      end: new FormControl(null, [Validators.required]),
      details: new FormControl(null, [Validators.required]),
      description: new FormControl(null, [Validators.required]),
      updates: new FormControl(null, [Validators.required]),
      nextInfo: new FormControl(null, [Validators.required]),
      ticketSystem: new FormControl(null, [Validators.required])
    });
  }

  public searchActivity(_obj: Observable<NotificationInfo[]>): void {
    this._notificationInfo$ = _obj;
  }

  public selectedActivity(_obj: NotificationInfo): void {
    this.isSearchPanelVisible = false;
    this.activityFormSubscriptions(_obj);
  }

  public activityFormSubscriptions(_obj: NotificationInfo): void {
    this.activityForm.controls.application.setValue(_obj.app);
    this.activityForm.controls.environment.setValue(_obj.env);
    this.activityForm.controls.start.setValue(_obj.startTime);
    this.activityForm.controls.end.setValue(_obj.endTime);
    this.activityForm.controls.activity.setValue(_obj.activity);
    this.activityForm.controls.description.setValue(_obj.description);
    this.activityForm.controls.details.setValue(_obj.details);
    this.activityForm.controls.nextInfo.setValue(_obj.nextInfo);
    this.activityForm.controls.ticketSystem.setValue(_obj.ticketSystem);
    this.activityForm.controls.updates.setValue(
      this.formatUpdateFieldValue(_obj.updates)
    );
  }

  private formatUpdateFieldValue(val: string[]): string {
    this.updateFormFieldValues = val;
    let str: string;
    if (val) {
      this._htmlView = this.htmlUpdateFieldView(val);
      val.map((item) => {
        if (str) {
          str = str + " | " + item;
        } else {
          str = item;
        }
      });
    }
    return str;
  }

  private htmlUpdateFieldView(_obj: string[]): string {
    let str: string;
    _obj.forEach((val) => {
      if (str) {
        str = str + "<br>" + val;
      } else {
        str = val;
      }
    });
    return "<td>" + str + "</td>";
  }

  public previewUpdates(_isClicked: boolean): void {
    if (_isClicked) {
      const newVal: string[] = [];
      const arr = this.activityForm.controls.updates.value.split(/\|/);
      let count = 1;
      arr.forEach((val) => {
        if (val.indexOf("Update") == -1) {
          const item = "Update-" + count + ": " + val;
          newVal.push(item);
        } else {
          newVal.push(val);
        }
        count = count + 1;
      });
      if (newVal.length > 0) {
        this.updateFormFieldValues = newVal;
        this._htmlView = this.htmlUpdateFieldView(newVal);
      }
    }
  }
}
