import { Component, Input } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { CONSTANTS } from "../../../shared-modules/constants";
import { APIConnector } from "../../../shared-modules/api-connector";
import { NotificationInfo } from "src/app/models/notification-info.model";

@Component({
  selector: "app-activity-extension-template",
  templateUrl: "./activity-extension-template.component.html",
})
export class ActivityExtensionTemplateComponent {
  @Input() mainForm: FormGroup;
  @Input() updateFieldValues: string[];
  @Input() htmlView: string;
  public headings = CONSTANTS.formHeadings;
  public isUpdate: boolean = false;
  constructor(private connector: APIConnector) {}

  public sendDetails(): void {
    const request: NotificationInfo = {
      app: this.mainForm.controls.application.value,
      env: this.mainForm.controls.environment.value,
      activity: this.mainForm.controls.activity.value,
      startTime: this.mainForm.controls.start.value,
      endTime: this.mainForm.controls.end.value,
      description: this.mainForm.controls.description.value,
      details: this.mainForm.controls.details.value,
      nextInfo: this.mainForm.controls.nextInfo.value,
      ticketSystem: this.mainForm.controls.ticketSystem.value,
      updates: this.updateFieldValues,
      htmlUpdate: this.htmlView 
    };
    this.connector.sendUpdates(request);
  }
}
