import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { ActivityExtensionsContainerComponent } from "./activity-extensions-container/activity-extensions-container.component";
import { SharedModule } from "../shared-modules/shared-module";
import { ExtensionActivitiesDisplayComponent } from "./activity-extensions-container/extension-search-activities/extension-activities-display.component";
import { NgbAccordionModule } from "@ng-bootstrap/ng-bootstrap";
import { ActivityExtensionTemplateComponent } from "./activity-extensions-container/activity-extension-template/activity-extension-template.component";

@NgModule({
    declarations:[
        ActivityExtensionsContainerComponent,
        ExtensionActivitiesDisplayComponent,
        ActivityExtensionTemplateComponent
    ],
    exports:[
        FormsModule,
        ReactiveFormsModule
    ],
    imports:[
        FormsModule,
        BrowserModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: "never" }),
        SharedModule,
        NgbAccordionModule
    ]
})
export class ActivityExtensionsModule {}
