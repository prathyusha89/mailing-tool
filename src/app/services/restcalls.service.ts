import { Injectable, OnDestroy } from "@angular/core";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from "@angular/common/http";
import { NotificationInfo } from "../models/notification-info.model";
import { CONSTANTS } from "../shared-modules/constants";
import { Observable, of, Subject, throwError } from "rxjs";
import { catchError, takeUntil } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class RestcallsService implements OnDestroy {

  /* private listData: NotificationInfo[] = [
     {
       app: "CUP",
       env: "TEST-A",
       activity: "Planned Downtime",
       description: "CUP R1.3.0",
       startTime: "2021-10-30 09:00",
       endTime: "20271-10-30 10:00",
       details: "60 min Downtime",
       subject: "Testing Activity-I",
     },
     {
       app: "CUP",
       env: "TEST-B",
       activity: "Planned Downtime",
       description: "CUP R1.4.0",
       startTime: "2021-11-30 09:00",
       endTime: "20271-11-30 10:00",
       details: "30 min Downtime",
       subject: "Testing Activity-II",
     },
     {
       app: "CUP",
       env: "TEST-A",
       activity: "Planned Downtime",
       description: "CUP R1.4.8",
       startTime: "2021-11-31 09:00",
       endTime: "20271-11-31 10:00",
       details: "90 min Downtime",
       subject: "Testing Activity-III",
     },
     {
       app: "CUP",
       env: "TEST-A",
       activity: "Planned Downtime",
       description: "CUP R1.4.8",
       startTime: "2021-11-31 09:00",
       endTime: "20271-11-31 10:00",
       details: "90 min Downtime",
     },
   ];*/

  destroy$: Subject<any> = new Subject();

  constructor(private http: HttpClient) { }

  public getAllCupJobs(): void {
    this.http
      .get<NotificationInfo[]>(CONSTANTS.apiGateway.getAllCupJobs)
      .subscribe((data) => {
        console.log(data);
      });
  }

  public searchActivities(
    startDate: any,
    endDate: any
  ): Observable<NotificationInfo[]> {
    return this.http
      .get<NotificationInfo[]>(CONSTANTS.apiGateway.getAllActivity, {
        params: { startDate, endDate },
      })
      .pipe(catchError(this.errorHandler));
    //return of(this.listData);
  }

  public dashboardActivities(): Observable<NotificationInfo[]> {
    return this.http
      .get<NotificationInfo[]>(CONSTANTS.apiGateway.dashboardgetAllActivity)
      .pipe(catchError(this.errorHandler));
  }

  public sendMail(info: NotificationInfo) {
    return this.http
      .post<NotificationInfo>(CONSTANTS.apiGateway.postFormUrl, info, {
        headers: new HttpHeaders({
          "Content-type": "application/json",
        }),
      })
      .pipe(catchError(this.errorHandler), takeUntil(this.destroy$))
      .subscribe((data) => {
        console.log(data);
      });
  }

  public sendReminder(info: NotificationInfo): void {
    this.http
      .post<NotificationInfo>(CONSTANTS.apiGateway.postReminderUrl, info, {
        headers: new HttpHeaders({
          "Content-type": "application/json",
        }),
      })
      .pipe(catchError(this.errorHandler), takeUntil(this.destroy$))
      .subscribe(() => { });
  }

  public sendExtension(info: NotificationInfo): void {
    this.http.post<NotificationInfo>(
      CONSTANTS.apiGateway.postExtension,
      info, {
      headers: new HttpHeaders({
        "Content-type": "application/json",
      }),
    })
      .pipe(catchError(this.errorHandler), takeUntil(this.destroy$))
      .subscribe(() => { });
  }

  public end1(info: NotificationInfo) {
    return this.http.post<NotificationInfo>(CONSTANTS.apiGateway.postEnd, info, {
      headers: new HttpHeaders({
        "Content-type": "application/json",
      }),
    })
      .pipe(catchError(this.errorHandler), takeUntil(this.destroy$))
      .subscribe(() => { });
  }

  private errorHandler(errorResponse: HttpErrorResponse) {
    if (errorResponse.error instanceof ErrorEvent) {
      console.log("Client Side Error:", errorResponse.error.message);
    } else {
      console.log("Server Side Error:", errorResponse);
    }
    return throwError(CONSTANTS.serverErrorMessage);
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
