import { Component, Input, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { NotificationInfo } from "../models/notification-info.model";
import { RestcallsService } from "../services/restcalls.service";

@Component({
  selector: "app-create",
  templateUrl: "./create.component.html",
  styleUrls: ["./create.component.css"],
})
export class CreateComponent implements OnInit  {
  
  public mainForm: FormGroup; 

  constructor(private service: RestcallsService) {}

  ngOnInit(): void {
      this.initForm();
  }

  public initForm(): void {
    this.mainForm = new FormGroup({
      application: new FormControl(null, [Validators.required]),
      environment: new FormControl(null, [Validators.required]),
      activity: new FormControl(null, [Validators.required]),
      start: new FormControl(null, [Validators.required]),
      end: new FormControl(null, [Validators.required]),
      details: new FormControl(null, [Validators.required]),
      description: new FormControl(null, [Validators.required]),
    });
  }
  
  public submitForm() {
    
    const request: NotificationInfo = {
       app: this.mainForm.controls.application.value,
       env: this.mainForm.controls.environment.value,
       activity: this.mainForm.controls.activity.value,
       startTime: this.mainForm.controls.start.value,
       endTime: this.mainForm.controls.end.value,
       description: this.mainForm.controls.description.value,
       details: this.mainForm.controls.details.value
     };
    this.service.sendMail(request);
  }
}
