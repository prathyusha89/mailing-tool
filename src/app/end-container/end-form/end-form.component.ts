import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NotificationInfo } from '../../models/notification-info.model';
import { RestcallsService } from '../../services/restcalls.service';
import { CONSTANTS } from 'src/app/shared-modules/constants';

@Component({
  selector: 'app-end-form',
  templateUrl: './end-form.component.html'
})
export class EndFormComponent {
  @Input() mainForm: FormGroup;
  @Input() updateFieldValues: string[];
  @Input() htmlView: string;
  public headings = CONSTANTS.formHeadings;
  public isUpdate: boolean = false;

  constructor(private service: RestcallsService) { }

  public sendDetails(): void {
    const request: NotificationInfo = {
      app: this.mainForm.controls.application.value,
      env: this.mainForm.controls.environment.value,
      activity: this.mainForm.controls.activity.value,
      startTime: this.mainForm.controls.start.value,
      endTime: this.mainForm.controls.end.value,
      description: this.mainForm.controls.description.value,
      details: this.mainForm.controls.details.value,
      updates: this.updateFieldValues,
      htmlUpdate: this.htmlView 
    };
    this.service.end1(request);
  }

}
