import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NotificationInfo } from 'src/app/models/notification-info.model';
import { CONSTANTS } from 'src/app/shared-modules/constants';

@Component({
  selector: 'app-end-activities',
  templateUrl: './end-activities.component.html',
  styleUrls: ['./end-activities.component.scss']
})
export class EndActivitiesComponent{
  @Input() _notificationInfo: NotificationInfo[];
  @Output() _activityInfo: EventEmitter<any> =  new EventEmitter<any>();

  public title = CONSTANTS.activityHeadings;

  public selectActivity (obj: NotificationInfo): void {
    this._activityInfo.emit(obj);
  }
}
