import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndActivitiesComponent } from './end-activities.component';

describe('EndActivitiesComponent', () => {
  let component: EndActivitiesComponent;
  let fixture: ComponentFixture<EndActivitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndActivitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndActivitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
