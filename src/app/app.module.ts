import { NgModule } from "@angular/core";
import { ServiceWorkerModule } from "@angular/service-worker";
import { environment } from "../environments/environment";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HeaderComponent } from "./main/components/header/header.component";
import { HomeComponent } from "./main/components/home/home.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { LayoutModule } from "@angular/cdk/layout";
import { EndFormComponent } from "./end-container/end-form/end-form.component";
import { ErrorPageComponent } from "./error-page/error-page.component";
import { ReminderFormComponent } from "./reminder-container/reminder-form/reminder-form.component";
import { ReminderContainerComponent } from "./reminder-container/reminder-container.component";
import { CreateComponent } from "./create/create.component";
import { RestcallsService } from "./services/restcalls.service";
import { SharedModule } from "./shared-modules/shared-module";
import {ActivityExtensionsModule} from "./activity-extensions/activity-extensions-module";
import {ReminderActivitiesDisplayComponent} from "./reminder-container/reminder-activities/reminder-activities-display.component"
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { EndContainerComponent } from './end-container/end-container.component';
import { EndActivitiesComponent } from './end-container/end-activities/end-activities.component';


@NgModule({
  declarations: [
    AppComponent,
    EndFormComponent,
    ErrorPageComponent,
    ReminderContainerComponent,
    ReminderFormComponent,
    CreateComponent,
    HeaderComponent,
    HomeComponent,
    ReminderActivitiesDisplayComponent,
    EndContainerComponent,
    EndActivitiesComponent
  ],
  exports: [HeaderComponent, HomeComponent],
  imports: [
    SharedModule,
    ActivityExtensionsModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: "never" }),
    HttpClientModule,
    LayoutModule,
    AppRoutingModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ServiceWorkerModule.register("ngsw-worker.js", {
      enabled: environment.production,
    }),
    NgbModule
  ],
  providers: [RestcallsService],
  bootstrap: [AppComponent],
})
export class AppModule {}
