import { Component, Input, Output, EventEmitter } from "@angular/core";
import { NotificationInfo } from "../../models/notification-info.model";
import {CONSTANTS} from "../../shared-modules/constants"

@Component({
  selector: "app-reminder-activities-display",
  templateUrl: "./reminder-activities-display.component.html",
  styleUrls: ["./reminder-activities-display.component.scss"]
})
export class ReminderActivitiesDisplayComponent {
  @Input() _notificationInfo: NotificationInfo[];
  @Output() _activityInfo: EventEmitter<any> =  new EventEmitter<any>();

  public title = CONSTANTS.activityHeadings;

  public selectActivity (obj: NotificationInfo): void {
    this._activityInfo.emit(obj);
  }
}
