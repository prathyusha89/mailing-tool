import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Observable } from "rxjs";
import { NotificationInfo } from "../models/notification-info.model";
@Component({
  selector: "app-reminder-container",
  templateUrl: "./reminder-container.component.html",
})
export class ReminderContainerComponent implements OnInit {
  public _notificationInfo$: Observable<NotificationInfo[]>;
  public mainForm: FormGroup;
  public isSearchPanelVisible = true;

  ngOnInit(): void {
    this.initForm();
  }

  public initForm(): void {
    this.mainForm = new FormGroup({
      application: new FormControl(null, [Validators.required]),
      environment: new FormControl(null, [Validators.required]),
      activity: new FormControl(null, [Validators.required]),
      start: new FormControl(null, [Validators.required]),
      end: new FormControl(null, [Validators.required]),
      details: new FormControl(null, [Validators.required]),
      description: new FormControl(null, [Validators.required]),
    });
  }

  public searchActivity(object: Observable<NotificationInfo[]>): void {
    if (object) {
      this._notificationInfo$ = object;
    }
  }

  public selectedActivity(obj: any): void {
    this.isSearchPanelVisible = false;
    this.mainForm.controls.application.setValue(obj.app);
    this.mainForm.controls.environment.setValue(obj.env);
    this.mainForm.controls.start.setValue(obj.startTime);
    this.mainForm.controls.end.setValue(obj.endTime);
    this.mainForm.controls.activity.setValue(obj.activity);
    this.mainForm.controls.description.setValue(obj.description);
    this.mainForm.controls.details.setValue(obj.details);
  }
}
