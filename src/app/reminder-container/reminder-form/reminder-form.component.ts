import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NotificationInfo } from '../../models/notification-info.model';
import { RestcallsService } from '../../services/restcalls.service';
import {CONSTANTS} from '../../shared-modules/constants'
@Component({
  selector: 'app-reminder-form',
  templateUrl: './reminder-form.component.html'
})
export class ReminderFormComponent {
  
  @Input() mainForm: FormGroup; 
  public headings = CONSTANTS.formHeadings;

  constructor(private service: RestcallsService) {}
  
  public sendReminder() {
    
    const request: NotificationInfo = {
       app: this.mainForm.controls.application.value,
       env: this.mainForm.controls.environment.value,
       activity: this.mainForm.controls.activity.value,
       startTime: this.mainForm.controls.start.value,
       endTime: this.mainForm.controls.end.value,
       description: this.mainForm.controls.description.value,
       details: this.mainForm.controls.details.value
     };
    this.service.sendReminder(request);
  }
}