import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import { NotificationInfo } from "src/app/models/notification-info.model";
import { APIConnector } from "src/app/shared-modules/api-connector";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
})
export class HomeComponent {
  constructor(private router: Router, private connector: APIConnector) { }
  public notificationInfo$: Observable<NotificationInfo[]>;

  activities:any = [
    // {
    //   "subject": "[END]: CUP PROD:Impairments on 2022-02-12T20:55 to 2022-02-12T21:55",

    //   "app": "CUP",

    //   "env": "PROD",

    //   "activity": "Impairments",

    //   "description": "patches apply",

    //   "startTime": "2022-02-12T20:55",

    //   "endTime": "2022-02-12T21:55",

    //   "details": "1 hr downtime",

    //   "action": "update",

    //   "updates": [

    //     "Update-1: update one here ",

    //     "Update-2:  update two here"

    //   ],

    //   "nextInfo": "17: 30 UTC",

    //   "ticketSystem": "UDH-1234"

    // },

    // {

    //   "subject": "[NEW]: CUP PROD:Unplanned Downtime on 2022-02-28T13:05 to 2022-02-28T14:05",

    //   "app": "CUP",

    //   "env": "PROD",

    //   "activity": "Unplanned Downtime",

    //   "description": "testing activity",

    //   "startTime": "2022-02-28T13:05",

    //   "endTime": "Ongoing",

    //   "details": "downtime of 30mins",

    //   "action": "reminder",

    //   "updates": null,

    //   "nextInfo": null,

    //   "ticketSystem": null

    // }
  ];

  displayedColumns: string[] = ['subject', 'action'];
  dataSource = this.activities;

  public createNotification(): void {
    this.router.navigate(["/create"]);
  }

  public sendReminder(): void {
    this.router.navigate(["/reminder"]);
  }

  public activityExtensions(): void {
    this.router.navigate(["/extension"]);
  }

  public endNotification(): void {
    this.router.navigate(["/endform"]);
  }

  redirectScreen(rowData: any) {
    // if (rowData.action === 'reminder') {
    //   this.notificationInfo$ = rowData;
    //   this.router.navigate(["/reminder"]);
    // }
    // else if (rowData.action === 'update') {
    //   this.notificationInfo$ = rowData;
    //   this.router.navigate(["/extension"]);
    // }
  }

  ngOnit(): void {
    this.activities = this.connector.fetchIndashboardActivities();
  }

}
