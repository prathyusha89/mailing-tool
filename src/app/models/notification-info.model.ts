export interface NotificationInfo {
  app: string;
  env: string;
  activity: string;
  startTime: string;
  endTime: string;
  details?: String;
  impact?: string;
  description: string;
  updateTime?: string;
  updates?: string[];
  htmlUpdate?: string;
  subject?: string;
  nextInfo?: string;
  ticketSystem?: string;
  action?: string;
}
